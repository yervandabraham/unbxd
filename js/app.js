var url = 'http://search.unbxd.io/fb853e3332f2645fac9d71dc63e09ec1/demo-unbxd700181503576558/search?&q=';
var products = [];
var xhttp = new XMLHttpRequest();
xhttp.open("GET", url + '*', true);
xhttp.send();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var responseText = JSON.parse(this.responseText);
        console.log(responseText);
        products = responseText.response.products;
        document.getElementById("results-count").innerHTML = responseText.response.numberOfProducts + ' Results';
        document.getElementById('products-list').innerHTML = getListingHtml();
        if(responseText.banner && responseText.banner.banners && responseText.banner.banners[0] && responseText.banner.banners[0].imageUrl){
            document.getElementById('main-banner').innerHTML = '<img src="' + responseText.banner.banners[0].imageUrl + '" />';
        }
    }
};

window.addEventListener('resize', function () {
    document.getElementById('products-list').innerHTML = getListingHtml(products);
});

document.getElementById('search-button').addEventListener("click", function(){
    xhttp.open("GET", url + document.getElementById('search-term').value, true);
    xhttp.send();
});

document.getElementById("search-term").addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        document.getElementById("search-button").click();
    }
});
document.getElementById('facet1').innerHTML = getFacetStylesHtml("Styles");
document.getElementById('facet3').innerHTML = getFacetFieldsHtml("Colors");

function loadFacetStyle(id){
    document.getElementById('fsc'+id).classList.toggle('facet-active');
}
function getFacetStyleHtml(facet){
    var facetActive = '';
    var facetChecked = '';
    if(facet.active){
        facetActive = ' facet-active';
        facetChecked = ' checked="checked"';
    }
    var fscId = '';
    if(facet.id != undefined) {
        fscId = ' id="fsc' + facet.id + '"';
    }
    var html = '<div'+ fscId +' class="facet-style-column' + facetActive + '">';
    html += '<div class="facet-style-img"><img src="img/facets/' + facet.img + '.png" />';
    if(facet.id != undefined) {
        html += '<input id="cbx' + facet.id + '" type="checkbox" class="cbx-input"' + facetChecked + '/>';
        html += '<label class="cbx" for="cbx' + facet.id + '" onclick="loadFacetStyle(' + facet.id + ')"><div class="flip"><div class="front"></div>';
        html += '<div class="back"><svg width="16" height="14" viewBox="0 0 16 14"><path d="M2 8.5L6 12.5L14 1.5"></path></svg></div>';
        html += '</div></label>';
    }
    html += '</div>';
    if(facet.title != undefined) {
        html +='<div class="facet-style-column-title">' + facet.title + ' (' + facet.count + ')</div>';
    }
    html += '</div>';
    return html;
}
function getFacetStylesHtml(title){
    //using dummy data
    var facetStyle = [
        {
            "id": 1,
            "active": false,
            "img": 'bohemian',
            "title": 'Bohemian',
            "count": 92
        },{
            "id": 2,
            "active": true,
            "img": 'contemporary',
            "title": 'Contemporary',
            "count": 888
        },{
            "id": 3,
            "active": false,
            "img": 'outdoor',
            "title": 'Outdoor',
            "count": 187
        },{
            "id": 4,
            "active": false,
            "img": 'traditional',
            "title": 'Traditional',
            "count": 562
        },{
            "img": 'outdoor_half'
        },{
            "img": 'traditional_half'
        }
    ];

    var rows=3;
    var cols=2;

    var html = '<div class="caret"></div><div class="facet-title">' + title + '</div><div class="facet-body">';
    for(var i=0; i < rows; i++) {
        html += '<div class="facet-style-row">';
        for (var j = 0; j < cols; j++) {
            html += getFacetStyleHtml(facetStyle[cols*i+j]);
        }
        html += '</div>';
    }
    html += '<div class="overlay"></div></div><button class="more-button"><span class="more-button-text">More ' + title + ' +</span></button>';
    return html;
}

function getFacetFieldsItemHtml(facet){
    var html = '<div class="facet-fields-row">';
    html += '<div class="facet-fields-column-checkbox">';
    html += '<input id="fcbx' + facet.id + '" type="checkbox" class="cbx-input" />';
    html += '<label class="cbx" for="fcbx' + facet.id + '"><div class="flip"><div class="front"></div>';
    html += '<div class="back"><svg width="16" height="14" viewBox="0 0 16 14"><path d="M2 8.5L6 12.5L14 1.5"></path></svg></div>';
    html += '</div></label>';
    html += '</div>';
    html += '<div class="facet-fields-column-name">' + facet.title + '</div>';
    html += '<div class="facet-fields-column-count">' + facet.count + '</div>';
    html += '</div>';
    return html;
}
function getFacetFieldsHtml(title){
    //using dummy data
    var facetColors = [
        {
            "id": 1,
            "title": 'Red',
            "count": 1221
        },{
            "id": 2,
            "title": 'Blue',
            "count": 1601
        },{
            "id": 3,
            "title": 'Green',
            "count": 134
        },{
            "id": 4,
            "title": 'White',
            "count": 656
        },{
            "id": 5,
            "title": 'Yellow',
            "count": 11684
        }
    ];


    var html = '<div class="caret"></div><div class="info"></div><div class="facet-title">' + title + '</div><hr /><div class="facet-body">';
    for(var i=0; i < facetColors.length; i++) {
        html += getFacetFieldsItemHtml(facetColors[i]);
    }
    html += '</div><button class="more-button"><span class="more-button-text">More ' + title + ' +</span></button>';
    return html;
}
/**
 * Get product card html
 *
 * @param product
 * @returns {string}
 */
function getProductCardHtml(product){
    if(!product.rating){
        product.rating = getRandomInt(0,100);
    }
    if(!product.reviews){
        product.reviews = getRandomInt(0,999);
    }
    var html = '<div class="product-card">';
    html += '<img class="product-image" src="' + product.productImage + '" title="' + product.title + '" alt="' + product.title + '"/>';
    html += '<div class="product-info">';
    html += '<h3 class="product-name">' + product.title + '</h3>';
    html += '<div class="ratings-and-reviews">';
    html += '<div class="star-ratings-sprite"><span style="width:' + product.rating + '%" class="star-ratings-sprite-rating"></span></div>';
    html += '<div class="amount-of-reviews">(' + product.reviews + ')</div>';
    html += '</div>'; //end ratings-and-reviews
    html += '<div class="shipping-offer">Shipping Offer</div>';

    var currency = '$';
    var priceArray = product.displayPrice.split('.');
    var price = priceArray[0].replace(currency, '');
    html += '<div class="product-price">' + '<span class="price-top">' + currency + '</span><span class="price-base">' + price + '</span><span class="price-top">' + priceArray[1] + '</span></div>';
    var c = (product.color.length==1)?'':'s';
    var s = (product.size.length==1)?'':'s';
    html += '<div class="product-options">' + product.color.length + ' Color' + c + ' / ' + product.size.length + ' Size'+ s + '</div>';
    html += '<div class="wishlist"></div>';
    html += '</div>'; //end product-info
    html += '</div>'; //end product-card
    return html;
}

/**
 * Get listing html based on products
 *
 * @returns {string}
 */
function getListingHtml(){
    var html = '';
    var productCount = products.length;
    if(productCount > 0){
        var colounCount = getColounCount();
        for(var i=0; i < productCount; i++){
            if(i % colounCount == 0){
                html += '<div class="products-row">';
            }
            html += getProductCardHtml(products[i]);
            if(i % colounCount == colounCount - 1){
                html += '</div>';
            }
        }
        var emptyCardCount = Math.ceil(productCount / colounCount) * colounCount - productCount;
        if(emptyCardCount > 0){
            for(var j=0; j<emptyCardCount; j++){
                html += '<div class="product-empty-card"></div>';
            }
            html += '</div>';
        }
    }
    else {
        html = '<p class="empty-results">There are no products matching your selection.</p>';
    }
    return html;
}

/**
 * Get count of colouns based on window width
 *
 * @returns {number}
 */
function getColounCount(){
    var w = window.innerWidth;
    var sizes = [1400, 1024, 768];
    for(var i=0; i<sizes.length; i++){
        if(w > sizes[i])
            return 4-i;
    }
    return 1;
}

/**
 * Get random int number
 *
 * @param min
 * @param max
 * @returns {*}
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}