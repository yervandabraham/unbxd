Product Listing form Unbxd
============

Product listing page from scratch using the search API

+ It's not using any libraries like jQuery etc. It's built using vanilla HTML, JS, CSS only.
+ The page is responsive and using 1-4 columns for products listing based on window width.
+ It's modular with reusable js functions for product card, facets and other components
+ For the Product card is used the relevant data from the search response and remaining fields are hardcoded in HTML
+ Implemented static facets in HTML and CSS based on dummy data from JS
+ Searching in the input box is invoking an API call and updating the page with the search results.

Browser Support
---------------

Below are the browsers that support the techniques I'm using.

+ IE 10+
+ Firefox 2+
+ Chrome 4+
+ Safari 3.1+
+ Opera 12.1+
+ iOS 3.2+
